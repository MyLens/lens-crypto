import { Data } from '@mylens/lens-types';

/**
 * Format of the cryptographic signature function ultimately used to sign data.
 */
export type SignFunc = (dataToSign : string, privateKey: string) => string

/**
 * Signs a Data, proving that it hasn't been modified since signing.
 * 
 * @param privateKey The private key that corresponds to the publicKey contained on the Data 
 * @param data The data object itself, to be signed
 * @param signFunc The cryptographic signature function used to sign the data.
 * @returns A shallow copy of the data, containing the correct signature.
 */
export default function signData(privateKey : string, data: Data, signFunc: SignFunc) : Data {
    let stringToSign = data.id + data.name + data.type + data.data + data.publicKey 
    let signature = signFunc(stringToSign, privateKey)
    return Object.assign({}, data, {signature: signature})
}