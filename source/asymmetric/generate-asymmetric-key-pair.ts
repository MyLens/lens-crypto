import { ec as EllipticCurve } from 'elliptic';
const ecurve = new EllipticCurve('secp256k1');

/**
 * Generates a new asymmetric key pair, consisting of a publicKey and a privateKey.
 * 
 * @returns An object with a .publicKey and .privateKey. Use the privateKey
 *          to encrypt content that can only be decrypted with the publicKey
 */
export default function generateAsymmetricKeyPair(): { publicKey: string, privateKey: string} {

    let privSize = 0;
    let keyPair = ecurve.genKeyPair(); 
    while(privSize != 32){
        keyPair = ecurve.genKeyPair();
        privSize = keyPair.getPrivate().byteLength(); 
    }

    const publicKeyAsBuffer : Buffer = Buffer.from(keyPair.getPublic().encode('hex', false) as unknown as Buffer);
    const publicKeyAsString : string = publicKeyAsBuffer.toString();
    const privateKeyAsString : string = keyPair.getPrivate('hex')

    return {
        publicKey: publicKeyAsString,
        privateKey: privateKeyAsString, 
    };
}
