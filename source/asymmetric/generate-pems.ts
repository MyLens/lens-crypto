import KeyEncoder from 'key-encoder';

let keyEncoder = new KeyEncoder('secp256k1')

/**
 * Given hex encoded public and private keys for secp256k1, derive the PEM format. This is
 * useful if you want to use things like JSON web tokens and other libraries that need PEM
 * files instead of raw key values. 
 * @param publicKey - SECP256k1 hex encoded public key
 * @param privateKey - SECP256k1 hex encoded private key
 */
export default function generatePems(publicKey: string, privateKey: string): {publicPem: string, privatePem: string} {
    return {
        publicPem: keyEncoder.encodePublic(publicKey, 'raw', 'pem'), 
        privatePem:  keyEncoder.encodePrivate(privateKey, 'raw', 'pem'), 
    }
}