import { Data } from '@mylens/lens-types';

export type VerifyFunc = (dataToVerify : string, signature : string, publicKey: string) => boolean

/**
 * Verifies that a piece of data has a valid signature.
 * @param data The signed piece of data we wish to check. The data must contain both a valid signature, the correct publicKey, and to have not been changed since signing in order to return true. 
 * @param verifyFunc The cryptographic signature verification function used to verify the signature is correct.
 * @returns True if the data has not been modified, contains the correct publicKey and a valid signature. False otherwise.
 */
export default function verifyData(data: Data, verifyFunc: VerifyFunc) : boolean {
    let stringToVerify = data.id + data.name + data.type + data.data + data.publicKey
    return verifyFunc(stringToVerify, data.signature, data.publicKey)
  }
  
  