/**
 * The encrypted for of data. Data is encrypted using a symmetric key.
 */
export default interface EncryptedMetaData {
    /**
     * The encrypted content of the data
     */
    cipherText: string,
    /**
     * The initialization vector of the data
     */
    iv: string, 
}