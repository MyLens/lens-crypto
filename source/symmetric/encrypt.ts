import crypto from 'crypto';

import EncryptedMetaData from './encrypted-meta-data';
import ALGORITHM from './algorithm';

/**
 * Encrypts string content to an encrypted form.
 * 
 * @param key A symmetric key, typically created with generateSymmetricKey 
 * @param content The content to encrypt.
 * @returns The encrypted content, containing a cypherText and iv. 
 */
export default function encrypt(key: string, content: string): EncryptedMetaData {
  const iv = crypto.randomBytes(16)
  let keyBuf = Buffer.from(key, 'hex')
  const cipher = crypto.createCipheriv(ALGORITHM, keyBuf, iv);
  let encrypted = cipher.update(content, 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return {
      cipherText: encrypted, 
      iv: iv.toString('hex')
  }
}