import crypto from 'crypto';

/**
 * Generates a new symmetric key.
 * 
 * @returns A string that can be used to encrypt and decrypt symmetricly.
 */
export default function generateSymmetricKey() : string{
    return crypto.randomBytes(32).toString('hex');
}