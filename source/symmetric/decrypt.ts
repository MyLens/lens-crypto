import crypto from 'crypto';

import EncryptedMetaData from './encrypted-meta-data';
import ALGORITHM from './algorithm';

/**
 * Decrypts encrypted content to its original form.
 * 
 * @param key A symmetric key, typically created with generateSymmetricKey 
 * @param content The content to decrypt. Must contain a cipherText and iv.
 * @returns The original string content. 
 */
export default function decrypt(key: string, content: EncryptedMetaData) : string {
  let bufIv = Buffer.from(content.iv, 'hex')
  let bufKey = Buffer.from(key, 'hex')
  const decipher = crypto.createDecipheriv(ALGORITHM, bufKey, bufIv);
  let decrypted = decipher.update(content.cipherText, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
}