import encrypt from '../source/symmetric/encrypt';
import decrypt from '../source/symmetric/decrypt';
import generateSymmetricKey from '../source/symmetric/generate-symmetric-key';

test("We can encrypt and decrypt content using a symmetric key", async () => {
    for (let i = 0; i < 1e3; i++){
    const originalContent = "The quick brown fox jumps over the lazy dog.";
    const key = generateSymmetricKey();

    const encryptedContent = encrypt(key, originalContent);

    expect(encryptedContent).toHaveProperty('cipherText');
    expect(encryptedContent).toHaveProperty('iv');
    expect(encryptedContent.cipherText).not.toBe(originalContent);

    const newContent = decrypt(key, encryptedContent);

    expect(newContent).toBe(originalContent);
}
})