
import generateAsymmetricKeyPair from '../source/asymmetric/generate-asymmetric-key-pair';
import generatePems from '../source/asymmetric/generate-pems'; 

import * as jwt from 'jsonwebtoken'; 

test("We can properly generate and sign JWTs with PEM files", async () => {    
    // There was an issue with the way private keys were being generated. 
    // It did not manifest itself each time, but happened occasionally (probably when a bit wasn't set right).
    // This loop attempts to ensure the probability of it happening. 
    for (let i =0; i < 25; i++) { 
        let pair = generateAsymmetricKeyPair(); 
        let {publicPem, privatePem} = generatePems(pair.publicKey, pair.privateKey); 
        const token = jwt.sign({ foo: 'bar' }, privatePem, { algorithm: 'ES256'});
        try {
            jwt.verify(token, publicPem, { algorithms: ['ES256'] });
        } catch (e) {
            console.log('failed public PEM: ', publicPem); 
            console.log('Length in bytes of public', Buffer.from(pair.publicKey,'hex').length)
            console.log('failed private PEM: ', privatePem); 
            console.log('Length in bytes of private', Buffer.from(pair.privateKey,'hex').length)
            throw e; 
        } 
    }
})
