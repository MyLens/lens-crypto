import { Data } from '@mylens/lens-types';

import signData, { SignFunc } from '../source/sign-data';
import verifyData, { VerifyFunc } from '../source/verify-data';
import generateAsymmetricKeyPair from '../source/asymmetric/generate-asymmetric-key-pair';

test("We can sign data", async () => {

    const keyPair = generateAsymmetricKeyPair();

    const data : Data = {
        id: "123abc",
        name: "firstName",
        type: "Text",
        data: "Bob",
        publicKey: keyPair.publicKey,
        signature: "",
        verifiedBy: []
    };

    const signFunc : SignFunc = function (dataToSign: string, privateKey: string) : string {
        return `Looks good! ${keyPair.publicKey} ${dataToSign}`;
    };

    const verifyFunc : VerifyFunc = function (dataToVerify: string, signature: string, publicKey: string) : boolean {
        return signature == `Looks good! ${publicKey} ${dataToVerify}`
    };

    const signedData = signData(keyPair.privateKey, data, signFunc);

    expect(signedData).toHaveProperty('signature');
    expect(signedData.signature).not.toBe("");

    expect(verifyData(signedData, verifyFunc)).toBeTruthy();
    expect(verifyData(data, verifyFunc)).toBeFalsy();
})